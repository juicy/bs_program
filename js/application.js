$(document).ready(function(){
  anim_scroll('.anim_scroll');
  carusel ('.slider', 1, 700, '', '', '.wrap', 2500, '.points', 'a');
  $('a.show_hide').click(function() {
    var temp = $(this).attr('rel');
    $(this).attr('rel', $(this).html());
    $(this).html(temp);
    if ($(this).parent().prev().hasClass('video')) {
      $(this).parent().prev().toggleClass('hide');
    } else {
      $(this).parent().parent().prev().toggleClass('hide');
    }
    return false;
  });
});

$(window).load(function() {
  menu_f('.c-menu', '.content');
});

function anim_scroll (links) {
  if ($(links).length > 0) {
    $(links).each(function(index){
      $(this).click(function(){
        $('body, html').animate({
          scrollTop: $($(this).attr('to')).offset().top - 120
        }, 200);
        return false;
      });
    });
  }
}

var start_top_pos = 0;

function menu_f (block, container) {
  if ($(block).length > 0) {
    start_top_pos = $(block).offset().top;
    $(window).scroll(function() {
      if (check_top_position (block, container)) {
        $(block).removeClass('fixed').removeClass('bottom')
        return false;
      };
      if (check_bottom_position (block, container)) {
        $(block).removeClass('fixed').addClass('bottom')
        return false;
      };
      $(block).addClass('fixed').removeClass('bottom')
    });
  };
}

function check_top_position (block, container) {
  return $(window).scrollTop() < start_top_pos;
}

function check_bottom_position (block, container) {
  return $(window).scrollTop() > $(container).offset().top + $(container).height() - $(block).height()
}

function carusel (block, in_window, width, left, right, wrap, time, points, point) {
  var th = 0;
  var max = $(block).find('.item').length - in_window;
  var hover = false;

  setTimeout(function() {
    auto();
  }, time);

  $(block).hover(function() {
    hover = true;
  }, function() {
    hover = false;
  });

  $(block).find(left).click(function() {
    to(th - 1);
    return false;
  });
  $(block).find(right).click(function() {
    to(th + 1);
    return false;
  });

  function auto() {
    if (!hover) {
      to(th + 1);
    }
    setTimeout(function() {
      auto();
    }, time);
  }

  $(block).find(points).find(point).click(function(){
    to($(this).prevAll().length);
    return false;
  });

  function to (num) {
    if (num < 0)   { num = max };
    if (num > max) { num = 0 };
    $(block).find(points).find(point).removeClass('active');
    $(block).find(points).find(point + ':eq(' + num + ')').addClass('active');
    $(block).find(wrap).animate({
      'margin-left': num * -1 * width
    }, 500, function() {
      th = num;
    })
  }
}